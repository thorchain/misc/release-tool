lint-verbose:
	@golangci-lint run -v
# ------------------------------- GitLab ------------------------------- #

docker-gitlab-login:
	docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}

docker-gitlab-push:
	docker push ${CI_REGISTRY_IMAGE}:latest

docker-gitlab-build:
	docker build  -t ${CI_REGISTRY_IMAGE}:latest .
# ------------------------------------------------------------------ #
